﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
	// Transform of the camera to shake. Grabs the gameObject's transform
	// if null.
	public Transform camTransform;
	
	// How long the object should shake for.
	float shakeDuration = 0f;
	
	// Amplitude of the shake. A larger value shakes the camera harder.
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1.0f;
	
	Vector3 originalPos;
	
	void Awake()
	{
	}
	
	void Start()
	{
		if (camTransform == null)
		{
			//camTransform = GetComponent(typeof(Transform)) as Transform;
            camTransform = GameObject.Find("Main Camera").transform;
		}
	}

    public void Shake()
    {
	    originalPos = camTransform.position;
        shakeDuration = 0.5f;
    }

	void Update()
	{
		if (shakeDuration > 0)
		{
			camTransform.position = originalPos + Random.insideUnitSphere * shakeAmount * shakeDuration;
			shakeDuration -= Time.deltaTime * decreaseFactor;
		}
		else if(shakeDuration < 0.0f)
		{
			shakeDuration = 0f;
    	    camTransform.position = originalPos;
		}
	}
}