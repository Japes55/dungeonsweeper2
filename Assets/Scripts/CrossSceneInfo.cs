﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CrossSceneInfo {

    public static List<Vector3> GetlevelConfigs() 
    {
        List<Vector3> levelConfigs = new List<Vector3>();
        levelConfigs.Add(new Vector3(9,9,10)); //0.12345 bombs per cell //official minecraft map
        levelConfigs.Add(new Vector3(12,12,20)); //0.135
        levelConfigs.Add(new Vector3(6,20,17)); //0.16
        levelConfigs.Add(new Vector3(20,6,17)); //0.16
        levelConfigs.Add(new Vector3(16,16,40)); //0.15625 //official minecraft map
        levelConfigs.Add(new Vector3(30,16,99)); //0.20625 //official minecraft map

        return levelConfigs;
    }

    //note: the initialisation values here and those set in Reset() must agree
    public static int CurrentLevel = 0;
    public static Vector2 playerStartingPos = new Vector2((int)GetlevelConfigs()[0].x/2, -1);
    public static int PlayerHealth = 3;

    public static void Reset()
    {
        CurrentLevel = 0;
        playerStartingPos = new Vector2((int)GetlevelConfigs()[0].x/2, -1);
        PlayerHealth = 3;
    }

}
