﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellBehaviour : MonoBehaviour
{
    public GameObject Bomb;
    Collider _collider;
    GameObject _player;

    int _numNeighbouringBombs;
    GameObject _numberIndicator;
    public bool Revealed{get; private set;}
    GameObject _bomb; //can be null
    bool _hasStairs;

    List<CellBehaviour> _neighbours;

    GlobalGameLogic _globalGameLogic;

    //called once, when object is created
    void Awake()
    {
        _numNeighbouringBombs = 0;
        Revealed = false;
        _bomb = null;
        _neighbours = new List<CellBehaviour>();
        _hasStairs = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<Collider>();
        _player = GameObject.FindGameObjectWithTag("Player");
        _numberIndicator = transform.Find("Number").gameObject;
        _globalGameLogic = GameObject.Find("GlobalGameLogic").GetComponent<GlobalGameLogic>();

        if(_hasStairs) {
            transform.Find("Ground").gameObject.SetActive(false);
            transform.Find("StairCase").gameObject.SetActive(true);
        } else {
            transform.Find("Ground").gameObject.SetActive(true);
            transform.Find("StairCase").gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(!Revealed) {
            _numberIndicator.SetActive(false); //do this here so that it's active for 1 frame, so that renderer/material etc is set so that reveal() woks
            if (_collider.bounds.Contains(_player.transform.position) && !Revealed)
            {
                Reveal();
            }
        }
    }

    public void AddBomb()
    {
        _bomb = Instantiate(Bomb, transform.position, Quaternion.identity);
    }

    public void AddNeighbouringBomb()
    {
        _numNeighbouringBombs += 1;
    }

    public void AddNeighbour(CellBehaviour neighbour)
    {
        _neighbours.Add(neighbour);
    }

    public void Reveal()
    {
        if(!Revealed) {

            _numberIndicator.SetActive(true);
            _numberIndicator.GetComponent<NumberIndicatorScript>().SetNumber(_numNeighbouringBombs);
            GetComponent<MeshRenderer>().enabled = false;
            Revealed = true;

            if(_numNeighbouringBombs == 0) {
                foreach(var n in _neighbours) {
                    n.Reveal();
                }
            }

            if(_bomb != null) {
                _globalGameLogic.BombExplosion();
            }
        }
    }

    public bool HasBomb()
    {
        return _bomb != null;
    }

    public void SetStairs()
    {
        _hasStairs = true;
    }
}
