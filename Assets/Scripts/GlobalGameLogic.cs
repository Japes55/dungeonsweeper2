﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalGameLogic : MonoBehaviour
{
    public GameObject Player;
    public GameObject Cell;
    public GameObject Rock;
    public GameObject TrapDoor;
    public GameObject Stairs;
    public GameObject GameOverScreen;
    public GameObject WinScreen;

    public AudioSource AudioSource;
    //0: door opening
    //1 - 3: bombs
    //4,5: level start sound
    public AudioClip[] AudioClips;

    //map deets
    int _gridSizeX;
    int _gridSizeZ;
    const int _borderWidth = 9;
    int _numBombs;

    //player deets
    int _health;

    List<List<CellBehaviour>> _cells;
    GameObject _trapDoor;
    GameObject _player;
    Vector3 _exitPosition;

    List<GameObject> _healthHearts;

    // Start is called before the first frame update
    void Start()
    {
        SetupLevel();
        _healthHearts = new List<GameObject>();
        _healthHearts.Add(GameObject.Find("h1"));
        _healthHearts.Add(GameObject.Find("h2"));
        _healthHearts.Add(GameObject.Find("h3"));
        _healthHearts.Add(GameObject.Find("h4"));
        _healthHearts.Add(GameObject.Find("h5"));
    }

    //uses the info in CrossSceneInfo to build the level
    void SetupLevel()
    {
        GameOverScreen.SetActive(false);
        WinScreen.SetActive(false);

        Vector3 levelConfig = CrossSceneInfo.GetlevelConfigs()[CrossSceneInfo.CurrentLevel];

        _gridSizeX = (int)levelConfig.x;
        _gridSizeZ = (int)levelConfig.y;
        _numBombs = (int)levelConfig.z;

        if(_gridSizeX * _gridSizeZ <= _numBombs) {
            _numBombs = _gridSizeX * _gridSizeZ - 1;
        }

        //player
        //make sure we're on this map...
        if(CrossSceneInfo.playerStartingPos.x >= _gridSizeX) {
            CrossSceneInfo.playerStartingPos.x = _gridSizeX - 1;
        }
        if(CrossSceneInfo.playerStartingPos.y >= _gridSizeZ) {
            CrossSceneInfo.playerStartingPos.y = _gridSizeZ - 1;
        }
        Vector3 playerStartingPos = new Vector3(CrossSceneInfo.playerStartingPos.x, 0.5f, CrossSceneInfo.playerStartingPos.y);
        _player = Instantiate(Player, playerStartingPos, Quaternion.identity);

        //game map
        _cells = new List<List<CellBehaviour>>();
        for(int i = 0; i < _gridSizeX; i++) {
            _cells.Add(new List<CellBehaviour>());
            for(int j = 0; j < _gridSizeZ; j++) {
                var go = Instantiate(Cell, new Vector3(i, 0.5f, j), Quaternion.identity);
                _cells[i].Add(go.GetComponent<CellBehaviour>());
            }
        }

        //rock boundary
        for(int i = -_borderWidth; i < _gridSizeX + _borderWidth; i++) {
            for(int j = -_borderWidth; j < _gridSizeZ + _borderWidth; j++) {
                if( MustBeRock(i,j, playerStartingPos)) {
                    Instantiate(Rock, new Vector3(i, 0.5f, j), Quaternion.identity);
                }
            }
        }

        //set up neighbours
        for(int i = 0; i < _gridSizeX; i++) {
            for(int j = 0; j < _gridSizeZ; j++) {
                SetupNeighbours(i, j);
            }
        }

        List<Vector2> bombLocations = AddBombs(CrossSceneInfo.playerStartingPos);

        //place exit
        Vector2 randomPos = new Vector2(Random.Range(0, _gridSizeX), Random.Range(_gridSizeZ/2, _gridSizeZ)); //must be in back half
        while(bombLocations.Contains(randomPos)) {
            randomPos = new Vector2(Random.Range(0, _gridSizeX), Random.Range(0, _gridSizeZ));
        }
        _trapDoor = Instantiate(TrapDoor, new Vector3(randomPos.x, 0.0f, randomPos.y), Quaternion.identity);
        _cells[(int)randomPos.x][(int)randomPos.y].SetStairs();
        _exitPosition = new Vector3(randomPos.x, 0.5f, randomPos.y);

        //player health
        _health = CrossSceneInfo.PlayerHealth;

        PlayLevelStartSound();
    }

    bool MustBeRock(int i, int j, Vector3 playerPos)
    {
        int playerSpace = 2;
        return (i < 0 || i >= _gridSizeX || j < 0 || j >= _gridSizeZ) &&
                    (playerPos - new Vector3(i, 0.5f, j)).magnitude > playerSpace;
    }

    void SetupNeighbours(int posx, int posz)
    {
        for(int i = posx - 1; i <= posx + 1; i++) {
            for(int j = posz - 1; j <= posz + 1; j++) {
                if( (i != posx || j != posz) && 
                    i >= 0 && i < _gridSizeX && 
                    j >= 0 && j < _gridSizeZ) {
                    _cells[posx][posz].AddNeighbour(_cells[i][j]);
                }
            }
        }
    }

    List<Vector2> AddBombs(Vector2 playerStartingPos)
    {
        //determine bomb locations
        List<Vector2> bombLocations = new List<Vector2>();
        while(bombLocations.Count < _numBombs) {
            Vector2 pos = new Vector2(Random.Range(0, _gridSizeX), Random.Range(0, _gridSizeZ));
            //make sure no bombs are close to player
            if(!bombLocations.Contains(pos) && 
                (pos.x > playerStartingPos.x + 1 ||
                pos.y > playerStartingPos.y + 1 ||
                pos.x < playerStartingPos.x - 1 ||
                pos.y < playerStartingPos.y - 1)) {
                bombLocations.Add(pos);
            }
        }

        //add bombs
        foreach(var pos in bombLocations) {
            int posx = (int)pos.x;
            int posz = (int)pos.y;
            _cells[posx][posz].AddBomb();

            //inform neighbours of bomb
            for(int i = posx - 1; i <= posx + 1; i++) {
                for(int j = posz - 1; j <= posz + 1; j++) {
                    if( (i != posx || j != posz) && 
                        i >= 0 && i < _gridSizeX && 
                        j >= 0 && j < _gridSizeZ) {
                        _cells[i][j].AddNeighbouringBomb();
                    }
                }
            }
        }

        return bombLocations;
    }

    // Update is called once per frame
    void Update()
    {
        if(GameConditionMet()) {
            if(_trapDoor != null) {
                PlayDoorOpeningSound();
                Destroy(_trapDoor);
            }
            if(_player.transform.position == _exitPosition) {

                CrossSceneInfo.playerStartingPos = new Vector2(_player.transform.position.x, _player.transform.position.z);
                CrossSceneInfo.PlayerHealth = _health;
                CrossSceneInfo.CurrentLevel++;

                //win condition
                if(CrossSceneInfo.CurrentLevel >= CrossSceneInfo.GetlevelConfigs().Count) {
                    WinCondition();
                } else {
                    ReloadScene();
                }
            }
        }

        for(int i = 1; i <= _healthHearts.Count; i++ ) {
            _healthHearts[i-1].SetActive(_health >= i);
        }

        //lose condition
        if(_health <= 0) {
            LoseCondition();
        }
    }

    void WinCondition()
    {
        enabled = false; //stop updates from happening on this script
        WinScreen.SetActive(true);
    }

    void LoseCondition()
    {
        enabled = false; //stop updates from happening on this script
        GameOverScreen.SetActive(true);
        _player.GetComponent<PlayerControl>().Die();
        _player.GetComponent<Rigidbody>().AddExplosionForce(20, 
                                        _player.transform.position + new Vector3(Random.Range(-1.0f, 1.0f), 0.5f, Random.Range(-1.0f, 1.0f)), 
                                        2.0f);
    }

    public void Reset()
    {
        CrossSceneInfo.Reset();
        ReloadScene();
    }

    void ReloadScene()
    {
        Scene loadedLevel = SceneManager.GetActiveScene();
        SceneManager.LoadScene (loadedLevel.buildIndex);
    }

    bool GameConditionMet()
    {
        foreach(var cellRow in _cells) {
            foreach(var cell in cellRow) {
                if(!cell.HasBomb() && !cell.Revealed) {
                    return false;
                }
            }
        }
        return true;
    }

    public void BombExplosion()
    {
        PlayBombSound();
        gameObject.GetComponent<CameraShake>().Shake();
        _health--;
    }

    void PlayDoorOpeningSound()
    {
        AudioSource.volume = 1f;
        AudioSource.clip = AudioClips[0];
        AudioSource.Play();
    }

    void PlayBombSound()
    {
        AudioSource.volume = 1f;
        int index = Random.Range(1, 4);
        AudioSource.clip = AudioClips[index];
        AudioSource.Play();
    }

    void PlayLevelStartSound()
    {
        AudioSource.volume = 1f;
        int index = Random.Range(4, 6);
        AudioSource.clip = AudioClips[index];
        AudioSource.Play();
    }
}
