﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberIndicatorScript : MonoBehaviour
{
    public Texture[] NumberTextures;
    Renderer _renderer;

    // Start is called before the first frame update
    void Start()
    {
        _renderer = GetComponent<Renderer> ();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetNumber(int number)
    {
        if(_renderer == null) {
            _renderer = GetComponent<Renderer> (); //no idea why it is sometimes null here.  happens after dying a few times.
            //Debug.Log("had to re-get renderer.  it is now " + _renderer);
        }
        
        if(number < 1 || number > 8) {
            _renderer.enabled = false;
        } else {
            _renderer.material.SetTexture("_MainTex", NumberTextures[number - 1]);
        }
    }
}
