﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    GameObject _mainCamera;
    bool _isDead;

    void Start()
    {
        _mainCamera = GameObject.Find("Main Camera");
        _mainCamera.transform.position = new Vector3(transform.position.x, 9, transform.position.z);
        GetComponent<Rigidbody>().isKinematic = true;
        _isDead = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(_isDead) {
            return;
        }
        
        var posX = transform.position.x;
        var posZ = transform.position.z;
        if(PressingUp()) {
            posZ += 1;
        }
        if(PressingDown()) {
            posZ -= 1;
        }
        if(PressingLeft()) {
            posX -= 1;
        }
        if(PressingRight()) {
            posX += 1;
        }
        var dest = new Vector3(posX, transform.position.y, posZ);
        if(dest != transform.position) {
            if(!RockIsInTheWay(dest)) {
                transform.position = dest;
                _mainCamera.transform.position = new Vector3(transform.position.x, 9, transform.position.z);
            }
        }
    }

    bool RockIsInTheWay(Vector3 dest)
    {
        RaycastHit hit;
        LayerMask mask = LayerMask.NameToLayer("rock");
        mask = ~mask; //??
        if (Physics.Raycast(transform.position, dest - transform.position, out hit, 1, mask)){
            return true;
        }
        return false;
    }

    bool PressingUp()
    {
        return (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow));
    }

    bool PressingDown()
    {
        return (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow));
    }

    bool PressingLeft()
    {
        return (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow));
    }

    bool PressingRight()
    {
        return (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow));
    }

    public void Die()
    {
        GetComponent<Rigidbody>().isKinematic = false;
        _isDead = true;
    }
}
